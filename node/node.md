# node开发环境

> 环境搭建

## 1. 安装 nvm-windows

> [GitHub - coreybutler/nvm-windows: A node.js version management utility for Windows. Ironically written in Go.](https://github.com/coreybutler/nvm-windows)

### 1.1 配置下载源

```settings.txt
## 本示例 nvm 安装目录为：D:\softwares\NVM\nvm
##D:\softwares\NVM\nvm\settings.txt 增加一下配置
node_mirror: https://npmmirror.com/mirrors/node/
npm_mirror: https://npmmirror.com/mirrors/npm/
```

### 1.2 下载node

```cmd
## 1.查询nide版本
nvm list available
## 2.选择对应稳定版本安装
nvm install 18.20.4
## nvm list 查询安装的node版本列表
nvm list
## 3.使用下载的版本
nvm use 18.20.4
## 查看node是否安装成功, 输出对应版本号则安装成功
node -v 
npm -v
## 查看node镜像源
npm config get registry
## 更改node镜像源为国内的淘宝镜像源，依赖下载速度快
## 注意切换镜像源时最好先清空缓存 npm cache clean --force
npm config set registry https://registry.npmmirror.com
## 配置node的全局和缓存配置
### 创建一下目录,文件位置和名称可随意命名；把该路径配置到path系统变量中
D:\softwares\NVM\nvm\node_cache
D:\softwares\NVM\nvm\node_global
### 添加配置
npm config set prefix "D:\tools\nodejs\node_global"
npm config set cache "D:\softwares\NVM\nvm\node_cache"

```



